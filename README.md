# Debian presentation model

## Description
This is a presentation model made, based on a suggestion from Carlos Melara.
The design started at MiniDebConf 2024, Belo Horizonte, Brazil.
The project uses as base templates the [Juliette Taka](http://www.juliettetaka.com/fr)'s theme for Debian 12, called [Emerald](https://wiki.debian.org/DebianArt/Themes/Emerald).

The project is in early development. Everything done until now are some templates of different slides styles. Feel free to apply these styles in your projects related with Debian and Free Software. 

We encourage people to use and create another templates, the models can serve as base to people who want a more debian-focused look on their presentations, whether in community activities or not. The main objective with the model here is to grant to people a standard template with good looking background, color palette, reasonables font sizes and [Debian logo](https://www.debian.org/logos/) on it.

## Visuals
- [ ] Upload some screenshot

## Usage
You can just download it here and use as a normal presentation in [Libreoffice Impress](https://pt-br.libreoffice.org/descubra/impress/).
We are developing templates and styles inside the sofware to simplify font size and layout picking.

## Support
You can open an issue here or send me an e-mail at [maierjeff@riseup.net](mailto:maierjeff@riseup.net)

## Roadmap
- [ ] Make slides of every color background
- [ ] Turn Font sizes into styles in Libreoffice
- [ ] Turn layouts into Master's in Libreoffice
- [ ] Create a short guide for usage
- [ ] Create a more complex color combinations palette
- [ ] Create a full design system

## Contributing
Everyone is welcome

## Authors and acknowledgment
Jefferson Maier (Design)
Carlos Henrique Lima Melara (concept)

## License
CC-BY-SA-4.0 or GNU GPL-2.0+ (at recipient's choice) 
